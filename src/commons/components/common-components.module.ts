import { NgModule } from '@angular/core';
import { CommonModule } from '../../../node_modules/@angular/common';
import { ModalComponent } from '@commons/components/modal/modal.component';
import { OverlayComponent } from '@commons/components/overlay/overlay.component';
import { BoldButtonComponent } from '@commons/components/bold-button/bold-button.component';
import { SmartTableComponent } from './smart-table/smart-table.component';
import { HeaderBackgroundComponent } from './header-background/header-background.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ModalComponent,
    OverlayComponent,
    BoldButtonComponent,
    SmartTableComponent,
    HeaderBackgroundComponent
],
  exports: [
    ModalComponent
  ]
})
export class CommonComponentsModule {}
