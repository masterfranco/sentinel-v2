import { Component, OnInit } from '@angular/core';
import { BussinessService } from '@providers/services/bussiness.service';
import { PageEventsService } from '@providers/pages/page-events.service';

@Component({
  selector: 'app-main-footer',
  templateUrl: './main-footer.component.html'
})
export class MainFooterComponent implements OnInit {
  constructor(public service: BussinessService, public page: PageEventsService) {}

  ngOnInit() {}
}
