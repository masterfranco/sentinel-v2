import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BussinessService } from '@providers/services/bussiness.service';
import { PageEventsService } from '@providers/pages/page-events.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html'
})
export class MainLayoutComponent implements OnInit, AfterViewInit {
  constructor(public service: BussinessService, public page: PageEventsService) { }

  ngOnInit() { }

  ngAfterViewInit() { }
}
