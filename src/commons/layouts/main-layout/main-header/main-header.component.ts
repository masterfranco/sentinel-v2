import { Component, OnInit } from '@angular/core';
import { UserService } from '@providers/services/user.service';
import { PageEventsService } from '@providers/pages/page-events.service';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html'
})
export class MainHeaderComponent implements OnInit {
  constructor(private user: UserService, private page: PageEventsService) {}

  ngOnInit() {}

  logout($event?: any) {
    this.user.logout();
  }
}
