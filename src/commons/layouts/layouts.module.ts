import { NgModule } from '@angular/core';

import { MainLayoutComponent } from './main-layout/main-layout.component';
import { MainHeaderComponent } from './main-layout/main-header/main-header.component';
import { MainFooterComponent } from './main-layout/main-footer/main-footer.component';
import { RouterModule } from '../../../node_modules/@angular/router';
import { CommonModule } from '../../../node_modules/@angular/common';
import { ComponentsModule } from '@providers/components/components.module';

@NgModule({
  declarations: [
    MainLayoutComponent,
    MainHeaderComponent,
    MainFooterComponent
  ],
  imports: [RouterModule, CommonModule,
  ComponentsModule],
  exports: [
    MainLayoutComponent,
    MainHeaderComponent,
    MainFooterComponent
  ]
})
export class LayoutsModule {}
