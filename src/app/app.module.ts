import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes, Route } from '@angular/router';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { LayoutsModule } from '@commons/layouts/layouts.module';
import { MainLayoutComponent } from '@commons/layouts/main-layout/main-layout.component';
import { LoginPageComponent } from '@providers/pages/login/login-page.component';
import { PagesModule } from '@providers/pages/pages.module';
import { CommonModule } from '../../node_modules/@angular/common';
import { AccessGuardService } from '@providers/guards/access-guard.service';
import { ComponentsModule } from '@providers/components/components.module';
import { LoginGuardService } from '@providers/guards/login-guard.service';
import { IndexPageComponent } from '@providers/pages/index/index-page.component';
import { MyProductsComponent } from '@providers/pages/products/my-products.component';
import { MyReportComponent } from '@providers/pages/report/my-report.component';
import { ApiService } from '@providers/helpers/api.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { WhoTabComponent } from '@providers/pages/report/tabs/who/who-tab.component';
import { DisqussTabComponent } from '@providers/pages/report/tabs/disquss/disquss-tab.component';
import { IndexTabComponent } from '@providers/pages/report/tabs/index/index-tab.component';
import { ProductsComponent } from '@providers/pages/products/products/products.component';
import { AllProductsComponent } from '@providers/pages/products/all/all-products.component';
import { IndexReportComponent } from '@providers/pages/report/index-report/index-report.component';
import { EnterpriseReportComponent } from '@providers/pages/report/enterprise-report/enterprise-report.component';
import { EnterpriseDetailedComponent } from '@providers/pages/report/enterprise-detailed/enterprise-detailed.component';
import { EnterpriseLandingComponent } from '@providers/pages/report/enterprise-landing/enterprise-landing.component';
import { MypeComponent } from '@providers/pages/mype/mype.component';

const ProjectRoutes: Routes = [
  {
    path: '',
    component: LoginPageComponent,
    canActivate: [LoginGuardService]
  },
  {
    path: 'app',
    component: MainLayoutComponent,
    // canActivate: [AccessGuardService],
    children: [
      {
        path: '',
        component: IndexPageComponent
      },
      {
        path: 'inicio',
        component: IndexPageComponent
      },
      {
        path: 'reporte',
        component: MyReportComponent,
        loadChildren: '@providers/pages/pages.module#ReportRoutesModule'
      },
      {
        path: 'productos',
        component: MyProductsComponent,
        loadChildren: '@providers/pages/pages.module#ProductsRoutesModule'
      },
      {
        path: 'beneficios/reporte',
        component: IndexReportComponent
      },
      {
        path: 'beneficios/empresa',
        component: EnterpriseLandingComponent,
        children: [
          {
            path: 'reporte',
            component: EnterpriseReportComponent
          },
          {
            path: 'detallado',
            component: EnterpriseDetailedComponent
          }
        ]
      },
      {
        path: 'mype',
        component: MypeComponent
      },
    ]
  }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(ProjectRoutes, { enableTracing: true }),
    CommonModule,
    LayoutsModule,
    ComponentsModule,
    PagesModule
  ],
  providers: [HttpClientModule, AccessGuardService, LoginGuardService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
