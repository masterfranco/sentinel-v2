import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '../../../node_modules/@angular/router';
import { Observable } from '../../../node_modules/rxjs';
import { UserService } from '@providers/services/user.service';

@Injectable()
export class LoginGuardService implements CanActivate {
  constructor(private service: UserService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    if (this.service.userLogged) {
      this.router.navigate(['core/reposicion']);
    }
    return true;
  }
}
