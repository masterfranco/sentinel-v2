import { Component, OnInit } from '@angular/core';
import { PageEventsService } from '@providers/pages/page-events.service';

@Component({
  selector: 'app-my-products',
  templateUrl: './my-products.component.html'
})
export class MyProductsComponent implements OnInit {

  constructor(private page: PageEventsService) {
    this.page.bgActive = false;
    this.page.sticky = true;
  }

  ngOnInit() { }

}
