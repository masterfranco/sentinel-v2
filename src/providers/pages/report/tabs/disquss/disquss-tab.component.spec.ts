/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DisqussTabComponent } from './disquss-tab.component';

describe('DisqussTabComponent', () => {
  let component: DisqussTabComponent;
  let fixture: ComponentFixture<DisqussTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisqussTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisqussTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
