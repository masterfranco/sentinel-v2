import { Component, OnInit } from '@angular/core';
import { PageEventsService } from '@providers/pages/page-events.service';

@Component({
  selector: 'app-index-report',
  templateUrl: './index-report.component.html'
})
export class IndexReportComponent implements OnInit {

  constructor(private page: PageEventsService) { }

  ngOnInit() {
    this.page.sticky = true;
    this.page.bgActive = true;
  }

}
