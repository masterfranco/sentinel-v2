import { Component, OnInit } from '@angular/core';
import { PageEventsService } from '@providers/pages/page-events.service';

@Component({
  selector: 'app-my-report',
  templateUrl: './my-report.component.html'
})
export class MyReportComponent implements OnInit {

  constructor(private page: PageEventsService) {
    this.page.bgActive = true;
    this.page.sticky = true;
  }

  ngOnInit() { }
}
