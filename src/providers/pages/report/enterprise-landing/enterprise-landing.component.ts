import { Component, OnInit } from '@angular/core';
import { PageEventsService } from '@providers/pages/page-events.service';

@Component({
  selector: 'app-enterprise-landing',
  templateUrl: './enterprise-landing.component.html'
})
export class EnterpriseLandingComponent implements OnInit {

  constructor(private page: PageEventsService) { }

  ngOnInit() {
    this.page.sticky = true;
    this.page.bgActive = false;
  }

}
