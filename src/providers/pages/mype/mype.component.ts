import { Component, OnInit } from '@angular/core';
import { PageEventsService } from '@providers/pages/page-events.service';

@Component({
  selector: 'app-mype',
  templateUrl: './mype.component.html'
})
export class MypeComponent implements OnInit {

  constructor(private page: PageEventsService) { }

  ngOnInit() {
    this.page.sticky = true;
    this.page.bgActive = false;
  }
}
