/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MypeComponent } from './mype.component';

describe('MypeComponent', () => {
  let component: MypeComponent;
  let fixture: ComponentFixture<MypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
