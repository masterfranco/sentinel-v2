import { Component, OnInit } from '@angular/core';
import { PageEventsService } from '@providers/pages/page-events.service';

@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html'
})
export class IndexPageComponent implements OnInit {

  constructor(private page: PageEventsService) { }

  ngOnInit() {
    this.page.sticky = false;
  }

}
