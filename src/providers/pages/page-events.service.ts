import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PageEventsService {

  public sticky: boolean = false;
  public bgActive: boolean = false;
  constructor() { }
}
