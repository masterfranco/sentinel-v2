import { Component, OnInit } from '@angular/core';
import { Router } from '../../../../node_modules/@angular/router';
import { UserService } from '@providers/services/user.service';
import { ApiService } from '@providers/helpers/api.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html'
})
export class LoginPageComponent implements OnInit {
  constructor(private user: UserService, private router: Router, private api: ApiService) { }

  ngOnInit() { }

  auth($event?: any) {
    this.user.login();
    this.router.navigate(['core/reposicion']);
  }


  login($event: KeyboardEvent) {
    this.router.navigate(['app/inicio']);
  }
}
