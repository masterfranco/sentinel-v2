import { NgModule } from '@angular/core';
import { LoginPageComponent } from '@providers/pages/login/login-page.component';
import { ComponentsModule } from '@providers/components/components.module';
import { CommonComponentsModule } from '@commons/components/common-components.module';
import { IndexPageComponent } from '@providers/pages/index/index-page.component';
import { MyProductsComponent } from '@providers/pages/products/my-products.component';
import { MyReportComponent } from '@providers/pages/report/my-report.component';
import { ApiService } from '@providers/helpers/api.service';
import { RouterModule, Route, Routes } from '@angular/router';
import { WhoTabComponent } from '@providers/pages/report/tabs/who/who-tab.component';
import { DisqussTabComponent } from '@providers/pages/report/tabs/disquss/disquss-tab.component';
import { IndexTabComponent } from '@providers/pages/report/tabs/index/index-tab.component';
import { AllProductsComponent } from '@providers/pages/products/all/all-products.component';
import { ProductsComponent } from '@providers/pages/products/products/products.component';
import { IndexReportComponent } from '@providers/pages/report/index-report/index-report.component';
import { EnterpriseReportComponent } from '@providers/pages/report/enterprise-report/enterprise-report.component';
import { EnterpriseDetailedComponent } from '@providers/pages/report/enterprise-detailed/enterprise-detailed.component';
import { EnterpriseLandingComponent } from '@providers/pages/report/enterprise-landing/enterprise-landing.component';
import { MypeComponent } from '@providers/pages/mype/mype.component';

const reportRoutes: Routes = [
  {
    path: 'index',
    component: IndexTabComponent
  },
  {
    path: 'vieron-mi-reporte',
    component: WhoTabComponent
  },
  {
    path: 'discrepancias',
    component: DisqussTabComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(reportRoutes)
  ],
  declarations: []
})
export class ReportRoutesModule { }

// **************************************
const productRoutes: Routes = [
  {
    path: 'todos',
    component: AllProductsComponent
  },
  {
    path: 'contratados',
    component: ProductsComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(productRoutes)
  ],
  declarations: []
})
export class ProductsRoutesModule { }

// **************************************
@NgModule({
  imports: [
    RouterModule,
    ComponentsModule,
    CommonComponentsModule,
    ReportRoutesModule,
    ProductsRoutesModule
  ],
  declarations: [
    LoginPageComponent,
    IndexPageComponent,
    MyProductsComponent,
    // ******
    AllProductsComponent,
    ProductsComponent,
    // ******
    MyReportComponent,
    // ******
    IndexReportComponent,
    WhoTabComponent,
    DisqussTabComponent,
    IndexTabComponent,
    // ******
    EnterpriseLandingComponent,
    EnterpriseReportComponent,
    EnterpriseDetailedComponent,
    // ******
    MypeComponent
  ]
})
export class PagesModule { }
