export * from './ajuste.service';
import { AjusteService } from './ajuste.service';
export * from './oHNegativo.service';
import { OHNegativoService } from './oHNegativo.service';
export * from './reposicion.service';
import { ReposicionService } from './reposicion.service';
export * from './tiendas.service';
import { TiendasService } from './tiendas.service';
export const APIS = [AjusteService, OHNegativoService, ReposicionService, TiendasService];
