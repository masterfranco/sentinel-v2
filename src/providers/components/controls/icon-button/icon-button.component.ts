import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'control-icon-button',
  templateUrl: './icon-button.component.html'
})
export class IconButtonComponent implements OnInit {
  @Input('classElement') classElement;
  @Input('icon') icon;

  constructor() {}

  ngOnInit() {}
}
