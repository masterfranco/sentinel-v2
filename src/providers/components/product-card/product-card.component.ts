import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html'
})
export class ProductCardComponent implements OnInit {

  @Input('title') title: string;
  @Input('background') background: string;
  @Input('tooltip') tooltip: string;

  constructor() { }

  ngOnInit() { }

}
