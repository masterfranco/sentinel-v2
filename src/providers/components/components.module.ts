import { NgModule } from '@angular/core';
import { IconButtonComponent } from '@providers/components/controls/icon-button/icon-button.component';
import { SelectComponent } from '@providers/components/controls/select/select.component';
import { CommonModule } from '../../../node_modules/@angular/common';
import { ProductCardComponent } from './product-card/product-card.component';
import { FlagStatusComponent } from './flag-status/flag-status.component';
import { FlagHistoryComponent } from './flag-history/flag-history.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    IconButtonComponent,
    SelectComponent,
    ProductCardComponent,
    FlagStatusComponent,
    FlagHistoryComponent
],
  exports: [
    IconButtonComponent,
    SelectComponent
  ]
})
export class ComponentsModule { }
