/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { BussinessService } from './bussiness.service';

describe('Service: Bussiness', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BussinessService]
    });
  });

  it('should ...', inject([BussinessService], (service: BussinessService) => {
    expect(service).toBeTruthy();
  }));
});
