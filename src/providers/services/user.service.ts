import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userLogged: boolean;
  constructor() {
    this.userLogged = false;
  }

  login() {
    this.userLogged = true;
  }

  logout() {
    this.userLogged = false;
  }
}
